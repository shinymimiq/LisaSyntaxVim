#README
This is the vim syntax files for LISA+.

##Usage
Copy two folders to your "$HOME/.vim/", then restart vim.

##Progress
Only added several important keywords, need more time to extract information from the LISA+ reference menual, and improve keywords detection. Please feel free to give comments and advices to Tony.
