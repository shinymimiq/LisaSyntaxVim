" Vim syntax file
" Language:	LISA+
" Maintainer:   Tony Liu (tony.liu@arm.com)
" Last Change:	05 Jun 2015

if exists("b:current_syntax")
  finish
endif

" Read the Cpp syntax to start with
runtime! syntax/cpp.vim

syn keyword lisaStructure       component protocol resources properties composition behavior debug includes connection behaviour
syn keyword lisaAtt    slave master internal optional peer
syn keyword lisaResources   PARAMETER REGISTER MEMORY
syn match lisaPortS     display contained "<[^>]*>"
syn match lisaPort      display "port<.*>" contains=lisaPortS
syn keyword lisaSelf    self

hi def link lisaStructure       Structure
hi def link lisaAtt             StorageClass
hi def link lisaResources       Macro
hi def link lisaPort            Type
hi def link lisaPortS           String
hi def link lisaSelf            Type

let b:current_syntax = "lisa"

" vim: ts=8
